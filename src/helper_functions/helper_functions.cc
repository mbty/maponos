// This file is part of the maponos library - gitlab.com/mbty/maponos
#include "helper_functions.hh"
namespace maponos {
  // # Internal helper function
  void print_message_location(
    int argc, char *argv[], size_t pos_begin, size_t pos_end,
    const std::string &message, arvernus::log_buffer_spawner &spawner
  ) {
    std::string error_indicator;
    error_indicator.reserve(pos_end);
    error_indicator.append(pos_begin, ' ');
    error_indicator.append(pos_end - pos_begin - 1, '^');

    spawner << "[Args   ] "
      << arvernus::print_range(argv, argv + argc, " ") << arvernus::newline
      << "          " << error_indicator << arvernus::newline
      << "          " << message;
  }

  // # Error management
  void print_error_location(
    int argc, char *argv[], size_t pos_begin, size_t pos_end,
    const std::string &message
  ) {
    print_message_location(
      argc, argv, pos_begin, pos_end,
      message, arvernus::error
    );
  }

  void print_error_location(
    int argc, char *argv[],
    const contextualized_string &from, const contextualized_string &to,
    const std::string &message
  ) {
    print_message_location(
      argc, argv, from.pos_begin, to.pos_end, message, arvernus::error
    );
  }

  void print_error_location(
    int argc, char *argv[],
    const contextualized_string &wrong_part, const std::string &message
  ) {
    print_message_location(
      argc, argv, wrong_part.pos_begin, wrong_part.pos_end, message,
      arvernus::error
    );
  }

  void print_warning_location(
    int argc, char *argv[], size_t pos_begin, size_t pos_end,
    const std::string &message
  ) {
    print_message_location(
      argc, argv, pos_begin, pos_end, message, arvernus::warning
    );
  }
}
