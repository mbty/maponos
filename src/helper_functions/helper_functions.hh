// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_HELPER__FUNCTIONS_HELPER__FUNCTIONS_HH
#define MAPONOS_HELPER__FUNCTIONS_HELPER__FUNCTIONS_HH
// It contains functions that are useful to explain what is wrong with the
// input to the user, showing not only the diagnosis, but also the location of
// the error.
#include "../../externals/arvernus/src/arvernus.hh"
#include "../contextualized_string/contextualized_string.hh"
namespace maponos {
  // # Error management
  void print_error_location(
    int argc, char *argv[], size_t pos_begin, size_t pos_end,
    const std::string &message
  );
  void print_error_location(
    int argc, char *argv[],
    const contextualized_string &from, const contextualized_string &to,
    const std::string &message
  );
  void print_error_location(
    int argc, char *argv[],
    const contextualized_string &wrong_part, const std::string &message
  );

  void print_warning_location(
    int argc, char *argv[], size_t pos_begin, size_t pos_end,
    const std::string &message
  );
}
#endif
