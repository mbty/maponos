// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_SUBCOMMANDS_SUBCOMMAND_HH
#define MAPONOS_SUBCOMMANDS_SUBCOMMAND_HH
// It contains the declaration of the subcommand class. The subcommand
// represents a possible action that the program can achieve, which can be
// further precised through the use of options.
#include <functional>
#include <memory>
#include <optional>
#include <sstream>
#include <string>
#include <vector>
#include "../../externals/arvernus/src/arvernus.hh"
#include "../contextualized_string/contextualized_string.hh"
#include "../options/abstract_option.hh"
#include "../options/flags/information_flag/information_flag.hh"
namespace maponos {
  class subcommand {
    public:
      subcommand(
        const std::string &display_name, const std::string &description,
        const std::vector<std::string> &call_names,
        const std::vector<std::shared_ptr<const abstract_option>>
          &associated_options,
        std::function<bool(
          int argc, char *argv[], const contextualized_string &call_name,
          const std::vector<contextualized_string> &arguments
        )> arguments_validator = [] (
          int argc, char *argv[], const contextualized_string &,
          const std::vector<contextualized_string> &arguments
        ) {
          if (arguments.size() == 0) {
            return true;
          }
          print_error_location(
            argc, argv, arguments[0],
            arguments[arguments.size() - 1], "Unexpected arguments"
          );
          return false;
        }
      );

      // # Validation
      bool matches_name(const contextualized_string &name) const noexcept;
      bool validate_arguments(
        int argc, char *argv[], const contextualized_string &call_name,
        const std::vector<contextualized_string> &arguments
      ) const noexcept;

      // # Options retrieval
      const std::vector<std::shared_ptr<const abstract_option>>
        &get_associated_options() const noexcept;

      // # Help generation
      std::string get_subcommand_help() const;
      virtual void show_options_help() const;

    protected:
      const std::string display_name;
      const std::string description;
      const std::vector<std::string> call_names;
      std::vector<std::shared_ptr<const abstract_option>> associated_options;

      // # Arguments validation
      std::function<bool(
        int argc, char *argv[], const contextualized_string &call_name,
        const std::vector<contextualized_string> &arguments
      )> arguments_validator;
  };
}
#endif
