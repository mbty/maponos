// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_SUBCOMMANDS_DEFAULT__SUBCOMMAND_HH
#define MAPONOS_SUBCOMMANDS_DEFAULT__SUBCOMMAND_HH
// It contains the definition of the default_subcommand class. This class
// inherits subcommand, and the difference between them is that
// default_subcommand is not called explicitly, but rather when no matching
// standard subcommands where found.
// In order to make the existence of the subcommand system invisible for the
// programs which don't require it, the generated help carefully avoids hinting
// at its existence.
#include "subcommand.hh"
namespace maponos {
  class default_subcommand : public subcommand {
    public:
      // Use this constructor if other subcommands exist
      default_subcommand(
        const std::string &description,
        const std::vector<std::shared_ptr<const abstract_option>>
          &associated_options,
        std::function<bool(
          int argc, char *argv[], const contextualized_string &call_name,
          const std::vector<contextualized_string> &arguments
        )> arguments_validator = [] (
          int argc, char *argv[], const contextualized_string &,
          const std::vector<contextualized_string> &arguments
        ) {
          if (arguments.size() == 0) {
            return true;
          }
          print_error_location(
            argc, argv, arguments[0], arguments[arguments.size() - 1],
            "The default subcommand expects no arguments"
          );
          return false;
        }
      );

      // Use this constructor if no other subcommands exist
      default_subcommand(
        const std::vector<std::shared_ptr<const abstract_option>>
          &associated_options,
        std::function<bool(
          int argc, char *argv[], const contextualized_string &call_name,
          const std::vector<contextualized_string> &arguments
        )> arguments_validator = [] (
          int argc, char *argv[], const contextualized_string &,
          const std::vector<contextualized_string> &arguments
        ) {
          if (arguments.size() == 0) {
            return true;
          }
          print_error_location(
            argc, argv, arguments[0], arguments[arguments.size() - 1],
            "Unexpected arguments"
          );
          return false;
        }
      );

      // # Help generator
      void show_options_help() const override;

    private:
      const bool only_subcommand;
  };
}
#endif
