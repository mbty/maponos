// This file is part of the maponos library - gitlab.com/mbty/maponos
#include "default_subcommand.hh"
// It contains the definition of the members of the default_subcommand class -
// see header for details.
namespace maponos {
  default_subcommand::default_subcommand(
    const std::vector<std::shared_ptr<const abstract_option>>
      &associated_options,
    std::function<bool(
      int argc, char *argv[], const contextualized_string &call_name,
      const std::vector<contextualized_string> &arguments
    )> arguments_validator
  )
  : subcommand(
      "default subcommand", "", {}, associated_options, arguments_validator
    ),
    only_subcommand(true)
  { }

  default_subcommand::default_subcommand(
    const std::string &description,
    const std::vector<std::shared_ptr<const abstract_option>>
      &associated_options,
    std::function<bool(
      int argc, char *argv[], const contextualized_string &call_name,
      const std::vector<contextualized_string> &arguments
    )> arguments_validator
  )
  : subcommand(
      "default subcommand", description, {}, associated_options,
      arguments_validator
    ), only_subcommand(false)
  { }

  // # Help generator
  void default_subcommand::show_options_help() const {
    std::ostringstream buffer;

    // The reason for the presence of multiple [Args   ] tags is that we
    // need to ensure that a one (and only one) tag is displayed no matter
    // which path of execution is taken.
    if (!only_subcommand) {
      buffer << "[Args   ] Current subcommand: " << get_subcommand_help()
        << arvernus::newline;
    }

    if (associated_options.size() > 0) {
      if (only_subcommand) {
        buffer << "[Args   ] ";
      }
      else {
        buffer << "          ";
      }
      buffer << "Available options:" << arvernus::newline;
      for (size_t i = 0; i < associated_options.size() - 1; ++i) {
        buffer << "          - " << associated_options[i]->get_help()
          << arvernus::newline;
      }
      buffer << "          - "
        << associated_options[associated_options.size() - 1]->get_help();
    }
    else if (only_subcommand) {
      buffer << "[Args   ] This program does not accept any option";
    }
    else {
      buffer << "          This subcommand does not accept any option";
    }

    arvernus::message << buffer.str();
  }
}
