// This file is part of the maponos library - gitlab.com/mbty/maponos
#include "subcommand.hh"
// It contains the definition of the subcommand class' members - see header for
// details.
namespace maponos {
  subcommand::subcommand(
    const std::string &display_name, const std::string &description,
    const std::vector<std::string> &call_names,
    const std::vector<std::shared_ptr<const abstract_option>>
      &associated_options,
    std::function<bool(
      int argc, char *argv[], const contextualized_string &call_name,
      const std::vector<contextualized_string> &arguments
    )> arguments_validator
  )
  : display_name(display_name), description(description),
    call_names(call_names), associated_options(associated_options),
    arguments_validator(arguments_validator)
  {
    this->associated_options.emplace_back(
      std::make_shared<const information_flag>(
        "help", "shows help", std::vector<std::string>{"help"},
        std::bind(&subcommand::show_options_help, this)
      )
    );
  }

  // # Validation
  bool subcommand::matches_name(
    const contextualized_string &candidate
  ) const noexcept {
    for (const auto &name : call_names) {
      if (name == candidate) {
        return true;
      }
    }
    return false;
  }

  bool subcommand::validate_arguments(
    int argc, char *argv[], const contextualized_string &call_name,
    const std::vector<contextualized_string> &arguments
  ) const noexcept {
    return arguments_validator(argc, argv, call_name, arguments);
  }

  // # Options retrieval
  const std::vector<std::shared_ptr<const abstract_option>>
    &subcommand::get_associated_options() const noexcept
  {
    return associated_options;
  }

  // # Help generation
  std::string subcommand::get_subcommand_help() const {
    std::ostringstream buffer;

    buffer << display_name << " (\""
      << arvernus::print_range(call_names.begin(), call_names.end(), "\", \"")
      << "\"): " << description;

    return buffer.str();
  }

  void subcommand::show_options_help() const {
    std::ostringstream buffer;
    buffer << "[Args   ] Current subcommand: " << get_subcommand_help()
      << arvernus::newline;

    if (associated_options.size() > 0) {
      buffer << "          Available options:" << arvernus::newline;
      for (size_t i = 0; i < associated_options.size() - 1; ++i) {
        buffer << "          - "
          << associated_options[i]->get_help() << arvernus::newline;
      }
      buffer << "          - "
        << associated_options[associated_options.size() - 1]->get_help();
    }
    buffer << "          This subcommand does not accept any option";

    arvernus::message << buffer.str();
  }
}
