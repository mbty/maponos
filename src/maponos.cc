// This file is part of the maponos library - gitlab.com/mbty/maponos
#include "maponos.hh"
// It contains the bulk of the logic of the library.
// The only function visible from the outside is parse_arguments - see header.
namespace maponos {
  // # Local functions declaration (arguments parsing)
  bool parse_subcommand(
    int argc, char *argv[], size_t &current_index, size_t &current_pos,
    const subcommand *&match, const std::vector<subcommand> &subcommands,
    const class default_subcommand &default_subcommand
  );
  bool parse_options(
    int argc, char *argv[], size_t &current_index, size_t &current_pos,
    const std::vector<std::shared_ptr<const abstract_option>> &options,
    struct init_info &init_info,
    std::set<const abstract_option *> &already_used_options,
    bool only_one_option
  );
  bool options_post_parsing(
    const std::vector<std::shared_ptr<const abstract_option>> &options,
    struct init_info &init_info,
    const std::set<const abstract_option *> already_used_options
  );
  // ## Subcommands parsing
  void parse_subcommand_name(
    int argc, char *argv[], size_t &current_index, size_t &current_pos,
    const contextualized_string &subcommand_candidate,
    const subcommand *&match, const std::vector<subcommand> &subcommands,
    const class default_subcommand &default_subcommand
  );
  bool parse_subcommand_arguments(
    int argc, char *argv[], size_t &current_index, size_t &current_pos,
    const contextualized_string &call_name, const subcommand *match
  );
  bool search_subcommand_match(
    const contextualized_string &candidate, const subcommand *&match,
    const std::vector<subcommand> &subcommands
  );
  void show_subcommands_help(const std::vector<subcommand> &subcommands);
  contextualized_string to_unescaped_contextualized_string(
    const char *string, size_t pos_begin
  );
  // ## Options parsing
  bool parse_next_option(
    int argc, char *argv[], size_t &current_index, size_t &current_pos,
    bool only_option,
    const std::vector<std::shared_ptr<const abstract_option>> &options,
    struct init_info &init_info,
    std::set<const abstract_option *> &already_used_options
  );
  // ### Standard options management
  contextualized_string parse_next_option_name (
    char *argv[], size_t &current_index, size_t &current_pos
  );
  std::vector<contextualized_string> parse_next_option_arguments(
    int argc, char *argv[], size_t &current_index, size_t &current_pos
  );
  // ### Multiple short options management
  std::list<contextualized_string> parse_multiple_short_option_names(
    char *argv[], size_t &current_index, size_t &current_pos
  );
  bool validate_multiple_short_option_names(
    int argc, char *argv[], std::list<contextualized_string> &candidates,
    std::vector<const abstract_option *> &matches,
    const std::vector<std::shared_ptr<const abstract_option>> &options
  );
  bool validate_multiple_short_option_null_arguments(
    int argc, char *argv[], const std::list<contextualized_string> &call_names,
    const std::vector<const abstract_option *> &options,
    struct init_info &init_info,
    std::set<const abstract_option *> &already_used_options
  );
  // ### Classification
  bool next_option_accepts_arguments (char *argv[], size_t current_index);
  bool next_argument_is_not_an_option(char *argv[], size_t current_index);
  // ### Navigation
  void skip_to_next_option(
    int argc, char *argv[], size_t &current_index, size_t &current_pos
  );
  void skip_to_next_option_silent(
    int argc, char *argv[], size_t &current_index, size_t &current_pos
  );
  // ### Misc.
  bool check_if_one_option_only(int argc, char *argv[], size_t current_index);
  bool search_option_match(
    int argc, char *argv[],
    const contextualized_string &candidate, const abstract_option *&match,
    const std::vector<std::shared_ptr<const abstract_option>> &options
  );
  bool redundant_option_check(
    int argc, char *argv[],
    contextualized_string call_name, const abstract_option *match,
    std::set<const abstract_option *> &already_used_options
  );

  // # Global functions defintion
  // Returns success state.
  // This function is somewhat ugly but there's not really a way around it as
  // far as I can see: a lot of context has to be shared during the complex
  // progress through the contents of argv.
  bool parse_arguments(
    int argc, char *argv[], struct init_info &init_info,
    const class default_subcommand &default_subcommand,
    const std::vector<subcommand> &subcommands
  ) {
    // argv[0] corresponds to the executable name; we are not interested by it
    // and can therefore directly skip to the first "real" argument.
    size_t current_index = 1;
    // The cursor is placed after the command in argv[0]
    size_t current_pos = strlen(argv[0]) + 1;

    // * Subcommand parsing
    const subcommand *current_subcommand = nullptr;
    bool subcommands_parsing_success = parse_subcommand(
      argc, argv, current_index, current_pos,
      current_subcommand, subcommands, default_subcommand
    );

    // There is no way to have current_subcommand == nullptr at this point.
    // Rather, it points to the selected subcommand:
    // - appropriate one if first arg matches a subcommand
    // - default one if there is no first arg that is not an option, or if the
    //   first arg does not match any available option (in this case, it will be
    //   considered as an argument of the default subcommand, possibly causing a
    //   failure)
    // Anyways, the execution continues.
    // If there was a failure (which is interpreted as a wrong argument to a
    // given subcommand), the program won't run, but the options are parsed
    // regardless (but not necessarily as expected: it is possible to define an
    // option validator so that it succeeds when some subcommands arguments are
    // given and fail otherwise, and those might be the root of the error, or
    // for the options of the default subcommand to be used instead of those of
    // the mistyped desired one).

    // * Options parsing
    // Information flags have to be passed as the only arguments
    bool only_one_option = check_if_one_option_only(argc, argv, current_index);

    // ** Available options fetching
    const std::vector<std::shared_ptr<const abstract_option>> &options =
      current_subcommand->get_associated_options()
    ;
    std::set<const abstract_option *> already_used_options;

    // ** Options parsing core
    bool options_parsing_success = parse_options(
      argc, argv, current_index, current_pos, options, init_info,
      already_used_options, only_one_option
    );

    // ** Post parsing
    // Do not check post parsing conditions when using an information flag
    if (!(init_info.do_not_execute && only_one_option)) {
      options_parsing_success &= options_post_parsing(
        options, init_info, already_used_options
      );
    }

    // * Failure management
    // Spontaneous display of options help if something went wrong in the
    // options parsing
    if (!options_parsing_success) {
      current_subcommand->show_options_help();
    }

    // Spontaneous display of subcommands help if something went wrong in the
    // subcommand parsing, or in the options parsing if the subcommand is the
    // default one (that is because an obvious cause for it could be the fact
    // that the user is not aware of the existence of subcommands, or a
    // mistyped subcommand name), or if the only argument is "--help" (might be
    // used by an user to detect available subcommands).
    if (!subcommands_parsing_success
      || ((current_subcommand == &default_subcommand)
        && !options_parsing_success)
      || ((argc == 2) && (strcmp(argv[1], "---help") == 0))
    ) {
      show_subcommands_help(subcommands);
    }

    // * Conclusion
    return (subcommands_parsing_success && options_parsing_success);
  }

  // # Local functions definition
  bool parse_subcommand(
    int argc, char *argv[], size_t &current_index, size_t &current_pos,
    const subcommand *&match, const std::vector<subcommand> &subcommands,
    const class default_subcommand &default_subcommand
  ) {
    contextualized_string subcommand_candidate;
    if (argc > 1) {
      subcommand_candidate =
        to_unescaped_contextualized_string(argv[1], current_pos);
    }
    // parse_subcommand_name returns the selected subcommand in match
    parse_subcommand_name(
      argc, argv, current_index, current_pos,
      subcommand_candidate, match, subcommands, default_subcommand
    );

    // Program name if default subcommand, subcommand call name otherwise
    bool success;
    if (match == &default_subcommand) {
      const auto program_call_name = to_unescaped_contextualized_string(
        argv[0], 0
      );
      success = parse_subcommand_arguments(
        argc, argv, current_index, current_pos, program_call_name, match
      );
    }
    else {
      success = parse_subcommand_arguments(
        argc, argv, current_index, current_pos, subcommand_candidate, match
      );
    }

    return success;
  }

  bool parse_options(
    int argc, char *argv[], size_t &current_index, size_t &current_pos,
    const std::vector<std::shared_ptr<const abstract_option>> &options,
    struct init_info &init_info,
    std::set<const abstract_option *> &already_used_options,
    bool only_one_option
  ) {
    bool success = true;

    // Cast safety: argc is positive, max int < max size_t
    while (current_index < static_cast<size_t>(argc)) {
      success &= parse_next_option(
        argc, argv, current_index, current_pos, only_one_option,
        options, init_info, already_used_options
      );
    }

    return success;
  }

  bool options_post_parsing(
    const std::vector<std::shared_ptr<const abstract_option>> &options,
    struct init_info &init_info,
    const std::set<const abstract_option *> already_used_options
  ) {
    bool success = true;

    for (auto &option : options) {
      if (already_used_options.find(&*option) != already_used_options.end()) {
        success &= option->post_parsing(true, init_info);
      }
      else {
        success &= option->post_parsing(false, init_info);
      }
    }

    return success;
  }

  // ## Subcommands parsing
  void parse_subcommand_name(
    int argc, char *argv[], size_t &current_index, size_t &current_pos,
    const contextualized_string &subcommand_candidate,
    const subcommand *&match, const std::vector<subcommand> &subcommands,
    const class default_subcommand &default_subcommand
  ) {
    if (argc > 1) {
      // Escaping a string that could otherwise be a subcommand marks it
      // unambiguously as a subcommand argument
      bool is_escaped   = (argv[1][0] == '\\');
      bool is_an_option = (argv[1][0] == '-');
      if (!is_escaped && !is_an_option) {
        if (search_subcommand_match(subcommand_candidate, match, subcommands)) {
          ++current_index;
          current_pos += strlen(argv[1]) + 1;
          return;
        }
      }
    }

    // If no subcommand was found, use default
    match = &default_subcommand;
  }

  // call_name is the program name if match is the default subcommand
  // (subcommand call name otherwise)
  bool parse_subcommand_arguments(
    int argc, char *argv[], size_t &current_index, size_t &current_pos,
    const contextualized_string &call_name, const subcommand *match
  ) {
    std::vector<contextualized_string> subcommand_arguments;

    for (
      ;
      (current_index < static_cast<size_t>(argc))
        && argv[current_index][0] != '-';
      ++current_index
    ) { // Cast safety: argc is positive, max int < max size_t
      subcommand_arguments.push_back(to_unescaped_contextualized_string(
        argv[current_index], current_pos
      ));
      current_pos += strlen(argv[current_index]) + 1;
    }

    return (match->validate_arguments(
      argc, argv, call_name, subcommand_arguments
    ));
  }

  bool search_subcommand_match(
    const contextualized_string &candidate, const subcommand *&match,
    const std::vector<subcommand> &subcommands
  ) {
    for (const auto &subcommand : subcommands) {
      if (subcommand.matches_name(candidate)) {
        match = &subcommand;
        return true;
      }
    }
    return false;
  }

  void show_subcommands_help(const std::vector<subcommand> &subcommands) {
    if (subcommands.size() > 0) {
      std::ostringstream buffer;

      buffer << "[Args   ] Available subcommands:" << arvernus::newline;
      for (size_t i = 0; i < subcommands.size() - 1; ++i) {
        buffer << "          " << subcommands[i].get_subcommand_help()
          << arvernus::newline;
      }
      buffer << "          "
        << subcommands[subcommands.size() - 1].get_subcommand_help();

      arvernus::info << buffer.str();
    }
  }

  contextualized_string to_unescaped_contextualized_string(
    const char *string, size_t pos_begin
  ) {
    // Remove leading \ if present to support escaping -
    // XXX \ being an escape character in bash, write \\--arg instead of just
    // \--arg
    if (string[0] == '\\') {
      return {string + 1, pos_begin, strlen(string)};
    }
    else {
      return {string, pos_begin};
    }
  }

  // ## Options parsing
  // Precondition: current index is the index of an option
  bool parse_next_option(
    int argc, char *argv[], size_t &current_index, size_t &current_pos,
    bool only_option,
    const std::vector<std::shared_ptr<const abstract_option>> &options,
    struct init_info &init_info,
    std::set<const abstract_option *> &already_used_options
  ) {
    bool success = true;

    // If simple short or long
    if (next_option_accepts_arguments(argv, current_index)) {
      contextualized_string call_name =
        parse_next_option_name(argv, current_index, current_pos);

      const abstract_option *match;
      bool match_found =
        search_option_match(argc, argv, call_name, match, options);

      if (match_found) {
        std::vector<contextualized_string> option_arguments =
          parse_next_option_arguments(argc, argv, current_index, current_pos);
        success &= redundant_option_check(
          argc, argv, call_name, match, already_used_options
        );
        success &= match->validate_arguments(
          argc, argv, only_option, call_name, option_arguments, init_info
        );
      }
      else {
        skip_to_next_option(argc, argv, current_index, current_pos);
        success = false;
      }
    }
    else { // If multiple shorts
      size_t initial_pos = current_pos;
      std::list<contextualized_string> call_names =
        parse_multiple_short_option_names(argv, current_index, current_pos);
      std::vector<const abstract_option *> matches;
      success &= validate_multiple_short_option_names(
        argc, argv, call_names, matches, options
      );

      // Cast safety: argc is positive, max int < max size_t
      if (
        (current_index < static_cast<size_t>(argc))
          && next_argument_is_not_an_option(argv, current_index)
       ) {
        skip_to_next_option_silent(argc, argv, current_pos, current_index);
        print_error_location(
          argc, argv, initial_pos, current_pos,
          "Arguments can't be passed to options when specifying multiple short "
            "options at the same time"
        );
        success = false;
      }
      else {
        success &= validate_multiple_short_option_null_arguments(
          argc, argv, call_names, matches, init_info, already_used_options
        );
      }
    }

    return success;
  }

  // ### Standard options management
  // Precondition: current index is the index of an option
  contextualized_string parse_next_option_name(
    char *argv[], size_t &current_index, size_t &current_pos
  ) {
    size_t option_size = strlen(argv[current_index]);
    current_pos += option_size + 1;
    ++current_index;

    // Safe due to precondition
    if (argv[current_index - 1][1] != '-') {
      return {
        &argv[current_index - 1][1], current_pos - option_size - 1, option_size
      };
    }
    else {
      return {
        &argv[current_index - 1][2], current_pos - option_size - 1, option_size
      };
    }
  }

  std::vector<contextualized_string> parse_next_option_arguments(
    int argc, char *argv[], size_t &current_index, size_t &current_pos
  ) {
    std::vector<contextualized_string> arguments;

    // Cast safety: argc is positive, max int < max size_t
    while (
      (current_index < static_cast<size_t>(argc))
        && argv[current_index][0] != '-'
    ) {
      arguments.emplace_back(
        to_unescaped_contextualized_string(argv[current_index], current_pos)
      );
      current_pos += strlen(argv[current_index]) + 1;
      ++current_index;
    }

    return arguments;
  }

  // ### Multiple short options management
  // Precondition: current index is the index of a short option
  std::list<contextualized_string> parse_multiple_short_option_names(
    char *argv[], size_t &current_index, size_t &current_pos
  ) {
    // List used as random access removal is used further down the line
    // (unrecognized names are removed, then further diagnostic is proposed for
    // accepted ones)
    std::list<contextualized_string> result;

    size_t initial_pos = current_pos;

    for (size_t i = 1; argv[current_index][i] != '\0'; ++i) {
      result.emplace_back(
        std::string(1, argv[current_index][i]), initial_pos + i, 1
      );
      ++current_pos;
    }

    ++current_pos;
    ++current_index;

    return result;
  }

  bool validate_multiple_short_option_names(
    int argc, char *argv[], std::list<contextualized_string> &candidates,
    std::vector<const abstract_option *> &matches,
    const std::vector<std::shared_ptr<const abstract_option>> &options
  ) {
    bool success = true;

    for (auto it = candidates.begin(); it != candidates.end(); ) {
      const abstract_option *result;
      if (search_option_match(argc, argv, *it, result, options)) {
        matches.push_back(result);
        ++it;
      }
      else {
        // Error message displayed in search_option_match
        it = candidates.erase(it);
        success = false;
      }
    }

    return success;
  }

  bool validate_multiple_short_option_null_arguments(
    int argc, char *argv[], const std::list<contextualized_string> &call_names,
    const std::vector<const abstract_option *> &options,
    struct init_info &init_info,
    std::set<const abstract_option *> &already_used_options
  ) {
    bool success = true;

    auto call_names_it = call_names.begin();
    for (size_t i = 0; i < options.size(); ++i, ++call_names_it) {
      success &= redundant_option_check(
        argc, argv, *call_names_it, options[i], already_used_options
      );
      success &= options[i]->validate_arguments(
        argc, argv, false, *call_names_it, {}, init_info
      );
    }

    return success;
  }

  // ### Classification
  // Precondition: current index is the index of an option
  // This function checks whether the current option is a short or a long one:
  // a simple short option (e.g. "-a") accepts arguments, and so does a long
  // one (e.g. "--option"). However, multiple concatenated options (e.g.
  // "-abc") don't.
  bool next_option_accepts_arguments(char *argv[], size_t current_index) {
    // Safe due to precondition
    if (argv[current_index][1] == '-' || argv[current_index][1] == '\0') {
      return true;
    }
    else {
      return (strlen(argv[current_index]) == 2);
    }
  }

  // Precondition: there is a next argument
  bool next_argument_is_not_an_option(char *argv[], size_t current_index) {
    return (argv[current_index][0] != '-');
  }

  // ### Navigation
  void skip_to_next_option(
    int argc, char *argv[], size_t &current_index, size_t &current_pos
  ) {
    auto initial_pos = current_pos;
    skip_to_next_option_silent(argc, argv, current_index, current_pos);

    if (initial_pos != current_pos) {
      print_warning_location(
        argc, argv, initial_pos, current_pos + 1,
        "These arguments are ignored because of a previous error"
      );
    }
  }

  void skip_to_next_option_silent(
    int argc, char *argv[], size_t &current_index, size_t &current_pos
  ) {
    for (
      ;
      // Cast safety: argc is positive, max int < max size_t
      (current_index < static_cast<size_t>(argc))
        && argv[current_index][0] != '-';
      ++current_index
    ) {
      current_pos += strlen(argv[current_index]) + 1;
    }
  }

  // ### Misc.
  // Precondition: current index is the index of an option
  bool check_if_one_option_only(int argc, char *argv[], size_t current_index) {
    for (
      ++current_index;
      // Cast safety: argc is positive, max int < max size_t
      current_index < static_cast<size_t>(argc);
      ++current_index
    ) {
      if (argv[current_index][0] == '-') {
        return false;
      }
    }
    return true;
  }

  bool search_option_match(
    int argc, char *argv[],
    const contextualized_string &candidate, const abstract_option *&match,
    const std::vector<std::shared_ptr<const abstract_option>> &options
  ) {
    for (auto &option : options) {
      if (option->matches(candidate)) {
        match = &*option;
        return true;
      }
    }

    print_error_location(argc, argv, candidate, "Unrecognized option");
    return false;
  }

  bool redundant_option_check(
    int argc, char *argv[],
    contextualized_string call_name, const abstract_option *match,
    std::set<const abstract_option *> &already_used_options
  ) {
    if (already_used_options.find(match) != already_used_options.end()) {
      print_error_location(argc, argv, call_name, "Redundant option");
      return false;
    }
    already_used_options.insert(match);
    return true;
  }
}
