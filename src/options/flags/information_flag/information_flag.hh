// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_OPTIONS_FLAGS_INFORMATION__FLAG_INFORMATION__FLAG_HH
#define MAPONOS_OPTIONS_FLAGS_INFORMATION__FLAG_INFORMATION__FLAG_HH
// It contains the declaration of the information_flag class. This class
// represents a flag (argumentless option) which has to be called as the sole
// option and a blocks the execution of the program without causing any error.
// This behavior is useful for options giving information about the program
// such as "help" and "version" - hence the name.
#include <functional>
#include "../abstract_flag.hh"
namespace maponos {
  class information_flag : public abstract_flag {
    public:
      information_flag(
        const std::string &display_name, const std::string &description,
        const std::vector<std::string> &call_names,
        std::function<void()> informer
      );

      // # Validation
      bool validate_arguments(
        int argc, char *argv[], bool only_option,
        const contextualized_string &call_name,
        const std::vector<contextualized_string> &arguments,
        struct init_info &init_info
      ) const noexcept override;

    private:
      const std::function<void()> informer;
  };
}
#endif
