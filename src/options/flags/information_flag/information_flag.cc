// This file is part of the maponos library - gitlab.com/mbty/maponos
#include "information_flag.hh"
namespace maponos {
  information_flag::information_flag(
    const std::string &display_name, const std::string &description,
    const std::vector<std::string> &call_names, std::function<void()> informer
  )
  : abstract_flag(
      display_name, description + " [takes no argument] [information flag]",
      call_names, &init_info::do_not_execute
    ),
    informer(informer)
  { }

  // # Validation
  bool information_flag::validate_arguments(
    int argc, char *argv[], bool only_option,
    const contextualized_string &call_name,
    const std::vector<contextualized_string> &arguments,
    struct init_info &init_info
  ) const noexcept {
    bool success = true;

    if (only_option) {
      success &= abstract_flag::validate_arguments(
        argc, argv, only_option, call_name, arguments, init_info
      );
      if (success) {
        informer();
      }
    }
    else {
      print_error_location(
        argc, argv, call_name,
        "Information flags should never be called alongside other options"
      );
      abstract_flag::validate_arguments(
        argc, argv, only_option, call_name, arguments, init_info
      );
      success = false;
    }

    return success;
  }
}
