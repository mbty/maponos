// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_OPTIONS_FLAGS_ABSTRACT__FLAG_HH
#define MAPONOS_OPTIONS_FLAGS_ABSTRACT__FLAG_HH
// It contains the declaration of the abstract_flag class.
// This class is inherited by the concrete flag types, and is quite explicit.
// It inherits templatized_abstract_option with bool as a template type (either
// the flag is set or it is not).
#include "../abstract_option.hh"
namespace maponos {
  class abstract_flag : public templatized_abstract_option<bool> {
    public:
      abstract_flag(
        const std::string &display_name, const std::string &description,
        const std::vector<std::string> &call_names, bool init_info::* dest
      );

      // # Validation
      bool validate_arguments(
        int argc, char *argv[], bool only_option,
        const contextualized_string &call_name,
        const std::vector<contextualized_string> &arguments,
        struct init_info &init_info
      ) const noexcept override;
  };
}
#endif
