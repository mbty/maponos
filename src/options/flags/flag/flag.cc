// This file is part of the maponos library - gitlab.com/mbty/maponos
#include "flag.hh"
namespace maponos {
  flag::flag(
    const std::string &display_name, const std::string &description,
    const std::vector<std::string> &call_names, bool init_info::* dest
  )
  : abstract_flag(
      display_name, description + " [takes no arguments]", call_names, dest
    )
  { }
}
