// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_OPTIONS_FLAGS_FLAG_FLAG_HH
#define MAPONOS_OPTIONS_FLAGS_FLAG_FLAG_HH
// This class doesn't hold big surprises: when the flag is requested, mark the
// relevant field as true, end of story.
#include "../abstract_flag.hh"
namespace maponos {
  class flag : public abstract_flag {
    public:
      flag(
        const std::string &display_name, const std::string &description,
        const std::vector<std::string> &call_names, bool init_info::* dest
      );
  };
}
#endif
