// This file is part of the maponos library - gitlab.com/mbty/maponos
#include "abstract_flag.hh"
// It contains the definition of the members of the abstract_flag class - see
// header for details.
namespace maponos {
  abstract_flag::abstract_flag(
    const std::string &display_name, const std::string &description,
    const std::vector<std::string> &call_names, bool init_info::* dest
  )
  : templatized_abstract_option(display_name, description, call_names, dest)
  { }

  // # Validation
  bool abstract_flag::validate_arguments(
    int argc, char *argv[], bool, const contextualized_string &,
    const std::vector<contextualized_string> &arguments,
    struct init_info &init_info
  ) const noexcept {
    if (arguments.size() != 0) {
      print_error_location(
        argc, argv, arguments[0], arguments[arguments.size() - 1],
        "Flags take no arguments"
      );
      return false;
    }

    init_info.*dest = true;
    return true;
  }
}
