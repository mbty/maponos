// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_OPTIONS_ABSTRACT__OPTION_HH
#define MAPONOS_OPTIONS_ABSTRACT__OPTION_HH
// This file contains the declaration of the abstract_option class. As its name
// implies, this class is an abstract one, outlining the interface that an
// option has to abide by. A templatized_abstract_option inheriting it is then
// defined, since options will depend on different types. These types don't have
// an impact on the interface besides the constructors, and all options inherit
// it.
// The reason for the separation of the templatized version of the class and its
// parent is that vector of pointers to templatized types don't work for mixed
// types.
#include <algorithm>
#include <string>
#include <vector>
#include "../../../maponos_configuration/init_info.hh"
#include "../contextualized_string/contextualized_string.hh"
namespace maponos {
  class abstract_option {
    public:
      abstract_option(
        const std::string &display_name, const std::string &description,
        const std::vector<std::string> &call_names
      );
      virtual ~abstract_option() =default;

      // # Validation
      virtual bool validate_arguments(
        int argc, char *argv[], bool only_option,
        const contextualized_string &call_name,
        const std::vector<contextualized_string> &arguments,
        struct init_info &init_info
      ) const noexcept =0;
      virtual bool post_parsing(
        bool was_used, struct init_info &init_info
      ) const noexcept;
      bool matches(const contextualized_string &name) const noexcept;

      // # Help generation
      std::string get_help() const;

    protected:
      const std::string display_name;
      const std::string description;
      const std::vector<std::string> call_names;
  };

  template <typename T>
  class templatized_abstract_option : public abstract_option {
    public:
      templatized_abstract_option(
        const std::string &display_name, const std::string &description,
        const std::vector<std::string> &call_names, T init_info::* dest
      )
      : abstract_option(display_name, description, call_names), dest(dest)
      { }

      protected:
        T init_info::* dest;
    };
}
#endif
