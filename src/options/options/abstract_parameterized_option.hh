// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_OPTIONS_OPTIONS_ABSTRACT__PARAMETERIZED__OPTION_HH
#define MAPONOS_OPTIONS_OPTIONS_ABSTRACT__PARAMETERIZED__OPTION_HH
// abstract_parameterized_option represents the interface which
// parameterized_options have to abide by. A parameterized option is an option
// which takes arguments, as opposed to a flag. You could technically use this
// class to represent a flag, but that does not mean you should.
#include "../abstract_option.hh"
namespace maponos {
  template <typename T>
  class abstract_parameterized_option : public templatized_abstract_option<T> {
    public:
      abstract_parameterized_option(
        const std::string &display_name, const std::string &description,
        const std::vector<std::string> &call_names,
        std::function<bool(
          int argc, char *argv[], const contextualized_string &call_name,
          const std::vector<contextualized_string> &arguments, T &result
        )> validator, T init_info::* dest
      )
      : templatized_abstract_option<T>(
          display_name, description, call_names, dest
        ), validator(validator)
      { }

      // # Validation
      bool validate_arguments(
        int argc, char *argv[], bool, const contextualized_string &call_name,
        const std::vector<contextualized_string> &arguments,
        struct init_info &init_info
      ) const noexcept override {
        return (this->validator(
          argc, argv, call_name, arguments, init_info.*(this->dest)
        ));
      }

    protected:
      const std::function<bool(
        int argc, char *argv[], const contextualized_string &call_name,
        const std::vector<contextualized_string> &arguments, T &result
      )> validator;
  };
}
#endif
