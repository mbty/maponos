// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_OPTIONS_OPTIONS_PARAMETERIZED__OPTION_PARAMETERIZED__OPTION_HH
#define MAPONOS_OPTIONS_OPTIONS_PARAMETERIZED__OPTION_PARAMETERIZED__OPTION_HH
// It contains the definition of the parameterized_option class, which
// represents your standard, run-of-the-mill option. To compensate for its
// otherwise blandness, it offers support for default values which are passed to
// the appropriate field when the option was not called.
#include "../abstract_parameterized_option.hh"
namespace maponos {
  template <typename T>
  class parameterized_option : public abstract_parameterized_option<T> {
    public:
      parameterized_option(
        const std::string &display_name, const std::string &description,
        const std::vector<std::string> &call_names,
        std::function<bool(
          int argc, char *argv[], const contextualized_string &call_name,
          const std::vector<contextualized_string> &arguments, T &result
        )> validator, T init_info::* dest, const T &default_value
      )
      : abstract_parameterized_option<T>(
          display_name, description, call_names, validator, dest
        ),
        default_value(default_value)
      { }

      // # Validation
      bool post_parsing(
        bool was_used, struct init_info &init_info
      ) const noexcept override {
        if (!was_used) {
          init_info.*(this->dest) = default_value;
        }
        return true;
      }

      private:
        const T default_value;
    };
}
#endif
