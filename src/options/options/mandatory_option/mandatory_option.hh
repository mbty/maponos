// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_OPTIONS_OPTIONS_MANDATORY__OPTION_MANDATORY__OPTION_HH
#define MAPONOS_OPTIONS_OPTIONS_MANDATORY__OPTION_MANDATORY__OPTION_HH
// It contains the definition of the mandatory_option class, which represents an
// option which must be called successfully in order for the program to run.
// Otherwise, the user is scolded appropriately.
#include "../../../../externals/arvernus/src/arvernus.hh"
#include "../abstract_parameterized_option.hh"
namespace maponos {
  template <typename T>
  class mandatory_option : public abstract_parameterized_option<T> {
    public:
      mandatory_option(
        const std::string &display_name, const std::string &description,
        const std::vector<std::string> &call_names,
        std::function<bool(
          int argc, char *argv[], const contextualized_string &call_name,
          const std::vector<contextualized_string> &arguments, T &result
        )> validator, T init_info::* dest
      )
      : abstract_parameterized_option<T>(
          display_name, description + " [mandatory]", call_names, validator,
          dest
        )
      { }

      // # Validation
      bool post_parsing(
        bool was_used, struct init_info &
      ) const noexcept override {
        if (!was_used) {
          arvernus::error << "[Args   ] Mandatory option \""
            << abstract_option::call_names[0] << "\" not set";
          return false;
        }
        return true;
      }
  };
}
#endif
