// This file is part of the maponos library - gitlab.com/mbty/maponos
#include "abstract_option.hh"
// It contains the definition of the members of the abstract option class - see
// header for details.
#include "../../externals/arvernus/src/arvernus.hh"
namespace maponos {
  abstract_option::abstract_option(
    const std::string &display_name, const std::string &description,
    const std::vector<std::string> &call_names
  )
  : display_name(display_name), description(description), call_names(call_names)
  { }

  // # Global functions definition
  // ## Validation
  bool abstract_option::matches(
    const contextualized_string &name
  ) const noexcept {
    return (std::find(call_names.cbegin(), call_names.cend(), name) !=
      call_names.cend());
  }

  bool abstract_option::post_parsing(bool, struct init_info &) const noexcept {
    return true;
  }

  // # Help generation
  std::string abstract_option::get_help() const {
    std::ostringstream buffer;

    buffer << display_name << " (\""
      << arvernus::print_range(call_names.begin(), call_names.end(), "\", \"")
      << "\"): " << description;

    return buffer.str();
  }
}
