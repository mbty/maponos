// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_CONTEXTUALIZED__STRING_CONTEXTUALIZED__STRING_HH
#define MAPONOS_CONTEXTUALIZED__STRING_CONTEXTUALIZED__STRING_HH
// It contains the declaration of the contextualized_string class, i.e. a
// string and its two position markers. The position markers allow it to
// remember the position of the substring in the original string. Since there
// is no real original string, string_views can't be used instead.
// This class is useful for showing where an error happened in the user input,
// and is notably used in the print_error_location functions - see
// helper_functions.hh.
#include <ostream>
#include <string>
namespace maponos {
  class contextualized_string : public std::string {
    public:
      contextualized_string();
      contextualized_string(const std::string &string, size_t pos);
      contextualized_string(
        const std::string &string, size_t pos_begin, size_t length
      );

    private:
      // # Position markers
      size_t pos_begin;
      size_t pos_end;

      friend void print_error_location(
        int argc, char *argv[],
        const contextualized_string &wrong_part, const std::string &message
      );
      friend void print_error_location(
        int argc, char *argv[],
        const contextualized_string &from, const contextualized_string &to,
        const std::string &message
      );
  };
}
#endif
