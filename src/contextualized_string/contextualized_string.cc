// This file is part of the maponos library - gitlab.com/mbty/maponos
#include "contextualized_string.hh"
// It contains the definitions of the contextualized_string member functions -
// see header for more details.
namespace maponos {
  contextualized_string::contextualized_string()
  : std::string(), pos_begin(0), pos_end(0)
  { }

  contextualized_string::contextualized_string(
    const std::string &string, size_t pos
  )
  : std::string(string), pos_begin(pos), pos_end(pos + string.size() + 1)
  { }

  contextualized_string::contextualized_string(
    const std::string &string, size_t pos_begin, size_t length
  )
  : std::string(string), pos_begin(pos_begin), pos_end(pos_begin + length + 1)
  { }
}
