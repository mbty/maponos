// This file is part of the maponos library - gitlab.com/mbty/maponos
#ifndef MAPONOS_MAPONOS_HH
#define MAPONOS_MAPONOS_HH
// It contains the definition of the parse_arguments function, which is the star
// of this library.
#include <cstring>
#include <functional>
#include <list>
#include <set>
#include <vector>
#include "../externals/arvernus/src/arvernus.hh"
#include "../../maponos_configuration/init_info.hh"
#include "contextualized_string/contextualized_string.hh"
#include "helper_functions/helper_functions.hh"
#include "options/abstract_option.hh"
#include "options/flags/flag/flag.hh"
#include "options/flags/information_flag/information_flag.hh"
#include "options/options/mandatory_option/mandatory_option.hh"
#include "options/options/parameterized_option/parameterized_option.hh"
#include "subcommands/default_subcommand.hh"
#include "subcommands/subcommand.hh"
namespace maponos {
  // Returns true on success, false on failure
  bool parse_arguments(
    int argc, char *argv[], struct init_info &init_info,
    const class default_subcommand &default_subcommand =
      maponos::default_subcommand({}),
    const std::vector<subcommand> &subcommands = {}
  );
}
#endif
