// This file is part of the maponos library - gitlab.com/mbty/maponos
#include <functional>
#include <iterator>
#include <memory>
#include <string>
#include <vector>
#include "../maponos/externals/arvernus/src/arvernus.hh"
#include "../maponos/src/maponos.hh"
// It is an example.
using namespace maponos;
// The following subcommand is used when no subcommands are identified.
// The help option is added automatically.
// XXX Mind the possible ambiguity if the subcommand accepts parameters of the
// same form as another subcommand's call name: in this case, you should escape
// it.
const default_subcommand get_default_subcommand() {
  std::vector<std::shared_ptr<const abstract_option>> options;

  options.emplace_back(std::make_shared<const mandatory_option<bool>>(
    "example", "sample option, does nothing",
    std::vector<std::string>{"example", "e"},
    [] (
      int, char *[], const contextualized_string &,
      const std::vector<contextualized_string> &, bool &dest
    ) {
      dest = true;
      return true;
    }, &init_info::example
  ));

  options.emplace_back(
    std::make_shared<const parameterized_option<bool>>(
      "hello", "sample option, does nothing",
      std::vector<std::string>{"hello", "h"},
      [] (
        int, char *[], const contextualized_string &,
        const std::vector<contextualized_string> &, bool &dest
      ) {
        dest = true;
        return true;
      },
        &init_info::hello,
        false
    )
  );

  options.emplace_back(
    std::make_shared<const flag>(
      "flag", "sample flag, does nothing",
      std::vector<std::string>{"flag", "f"},
      &init_info::flag
    )
  );

  return options;
}

int main(int argc, char *argv[]) {
  struct maponos::init_info init_info;

  auto default_subcommand = get_default_subcommand();

  bool success = maponos::parse_arguments(
    argc, argv, init_info, default_subcommand
  );

  if (!success) {
    arvernus::message << "Failure detected";
  }
  else {
    arvernus::info
      << "do_not_execute: " << init_info.do_not_execute << arvernus::newline
      << "example: " << init_info.example << arvernus::newline
      << "flag: " << init_info.flag << arvernus::newline
      << "hello: " << init_info.hello;
  }
}
