// This file is part of the maponos library - gitlab.com/mbty/maponos
#pragma once
// You can customize the init_info struct to your heart's content as long as
// you keep the do_not_execute field in.
namespace maponos {
  struct init_info {
    bool do_not_execute = false; // Do not remove
    bool example        = false;
    bool flag           = false;
    bool hello          = false;
  };
}
