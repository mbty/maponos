# Examples
Each of the folders in here contain an `maponos_configuration` subfolder.
In order to test an example, the aforementioned subfolder of the example of your
choice ought to be copied to the folder containing the root of this library,
i.e. `../..`.
The recommended way to proceed is to go to `../build` and to use
`load_example.sh` with one of the allowed values (which should be `minimal`,
`minimal_lib` and `advanced`).

* `example_minimal_lib` contains only the basic definition of the `init_info`
structure and can be used to compile to a library. Use this as a template for
your programs as it lets you define your own entry point.
* `example_minimal` holds the same contents as `example_minimal_lib` plus a
`main.cc`, and can thus be used to compile an executable with only the help
option available.
* `example_advanced` makes use of several options of different types. Compiles
to an executable.
