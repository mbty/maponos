// This file is part of the maponos library - gitlab.com/mbty/maponos
#include <functional>
#include <iterator>
#include <memory>
#include <string>
#include <vector>
#include "../maponos/externals/arvernus/src/arvernus.hh"
#include "../maponos/src/maponos.hh"
// It is an example.
using namespace maponos;
int main(int argc, char *argv[]) {
  struct maponos::init_info init_info;

  // The help option is added automatically
  // By default, all arguments but the help option cause an error
  bool success = maponos::parse_arguments(argc, argv, init_info);

  if (!success) {
    arvernus::message << "Failure detected";
  }
  else {
    arvernus::info << "do_not_execute: " << init_info.do_not_execute;
  }
}
