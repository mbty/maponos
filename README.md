# ![logo](https://i.imgur.com/bBlFSoK.png) Maponos
Maponos is a simple (about 1200 LOC) C++ arguments parser.

## Supported formats
Subcommands, grouped at the beginning: anything that doesn't begin with `-`.

Subcommands can also be used to read in arguments like in `filemanager copy
source destination` where `source` and `destination` could be a more convenient
way to express the same idea as `filemanager copy --input source --output
destination`.

Options without argument:
* `--long` for long options
* `-s`     for short options
* `-sxv`   for multiple short options

Options with argument:
* `--long arg1 arg2 ...` for long options
* `-s arg1 arg2 ...` for short options

## Design goals
* Standard syntaxes support
* Arguments validation
* Helpful diagnosis
* Automatic help
* No bloat

## Syntax
All the functions are defined in the maponos namespace (so prepend their calls
with "maponos::").

 ```C++
bool parse_arguments(
  int argc, char *argv[], struct init_info &init_info,
  const class default_subcommand &default_subcommand = ...,
  const std::vector<subcommand> &subcommands = {}
);
```

Where false means that a fatal error happened and that execution should stop,
and true means that everything went all right.
As described in the following section, sometimes, even when no errors occurred,
the program should not execute in the usual way (that's related to options such
as `help`, where help should be shown but that's all).
The default subcommand is the subcommand that only supports the help option.

### init\_info
The `init_info` struct is defined in the
`maponos_configuration/maponos_configuration.hh` file.
However, as explained in that file, it as well as its containing folder should
be copied in the folder containing this repository, in such a way as to mark it
part of the right repository.
Initially, it contains a single field named `do_not_execute`, which should not
be removed and whose utility is further explained in the options section.

An `init_info` struct is passed by reference to and modified by the
`parse_arguments` function.  It contains the results of the parsing operation.

### Subcommands
A subcommand represents an action that the program can perform.
This action can be configured through the use of options.
Sometimes, the user might call a program without a subcommand, that is, either
with an option (anything starting with '-') as the first argument, or with an
argument (anything else) that does not match any subcommand.

It is done through a so-called `default_subcommand`.

Subcommands can take parameters (which will be passed as the arguments from the
subcommand's name up to the next option).

In case the default argument accepts parameters of the same form as another
subcommand's name, the ambiguity can be solved through escaping.

```C++
subcommand(
  const std::string &display_name, const std::string &description,
  const std::vector<std::string> &call_names,
  const std::vector<std::unique_ptr<const abstract_option>> &associated_options,
  std::function<bool(
    int argc, char *argv[], const contextualized_string &call_name,
    const contextualized_string &call_name,
    const std::vector<contextualized_string> &arguments
  )> arguments_validator = ...
);
```
`call_names` are the names that the user can use to call the subcommand.
The `call_name` argument in the `std::function` contains the program call name
if the subcommand is the default one, or the subcommand call name otherwise.
It is useful in situations where some arguments have to be passed to a program
or subcommand, but are missing.

```C++
default_subcommand(
  const std::string &description,
  const std::vector<std::unique_ptr<const abstract_option>> &associated_options,
  std::function<bool(
    int argc, char *argv[], const contextualized_string &call_name,
    const std::vector<contextualized_string> &arguments
  )> arguments_validator = ...
);

default_subcommand(
  const std::vector<std::unique_ptr<const abstract_option>> &associated_options,
    std::function<bool(
      int argc, char *argv[], const contextualized_string &call_name,
      const std::vector<contextualized_string> &arguments
    )> arguments_validator = ...
);
```
Where `arguments_validator` takes as default argument a function that refuses
parameters.

The first version should be used when other subcommands exist, and the second
version should be used otherwise (when there is only one subcommand, the fact
that it is one is transparent to the user and `description` is left unused).

### Options
An option is any element that implements the `abstract_option` class.

There are four default option types:
* Flags: take no arguments, binary present/absent result.
* Information flags: take no arguments, set `do_not_execute`, should always be
  called as the sole option as they block execution (examples: help, version).
  Help is an automatically generated information flag.
* Options: take arguments (managed by an user passed function), have a default
  value if they are not called.
* Mandatory options: same as options, but cause an error if they are not called.

```C++
flag(
  std::string display_name, std::string description,
  std::vector<std::string> call_names, bool init_info::* dest
);

information_flag(
  std::string display_name, std::string description,
  std::vector<std::string> call_names, std::function<void()> informer
);

template <typename T>
parameterized_option(
  std::string display_name, std::string description,
  std::vector<std::string> call_names,
  std::function<bool(
    int argc, char *argv[], const contextualized_string &call_name,
    const std::vector<contextualized_string> &arguments, T &result
  )> validator,
  T init_info::* dest, T default_value
);

template <typename T>
mandatory_option<T> (
  std::string display_name, std::string description,
  std::vector<std::string> call_names,
  std::function<bool(
    int argc, char *argv[], const contextualized_string &call_name,
    const std::vector<contextualized_string> &arguments, T &result
  )> validator,
  T init_info::* dest
);
```

### Contextualized strings
A contextualized string can be used just like a normal string.
It contains some additional data related to its original position in the
program's arguments.
Some additional functions make use of this facility:

```C++
void print_error_location(
  int argc, char *argv[], size_t pos_begin, size_t pos_end,
  const std::string &message
);

void print_error_location(
  int argc, char *argv[],
  const contextualized_string &from, const contextualized_string &to,
  const std::string &message
);

void print_error_location(
  int argc, char *argv[], const contextualized_string &wrong_part,
  const std::string &message
);

void print_warning_location(
  int argc, char *argv[], size_t pos_begin, size_t pos_end,
  const std::string &message
);
```

They can be used in custom options validation functions to display clear error
information.

## Ready-to-use validation functions
See [this repository](https://gitlab.com/mbty/maponos_validation_functions).

## Limitations
Requires at least C++17.

Relies on [Arvernus](https://gitlab.com/mbty/arvernus) for error messages
display (can easily be substituted by stderr).

Mind the fact that `--string "--no-display"` is indistinguishable from
`--string --no-display` from the point of view of the program, so escaping has
to be used (`--string \--no-display` is not enough if you are using bash, since
bash has an escape mechanism of its own - actually, in this case, it should be
`\\--no-display`). Escaping is only activated for the first character of every
argument.

No fancy tests, only somewhat battle-tested.

## Legal
This library is free software. It comes without any warranty, to the extent
permitted by applicable law. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2, included
in the LICENSE.md file.

## Future
I consider this project to be complete.

## Alternatives
* [getopt](https://www.gnu.org/software/libc/manual/html_node/Parsing-Program-Arguments.html#Parsing-Program-Arguments)
* [glib commandline option parser](https://developer.gnome.org/glib/stable/glib-Commandline-option-parser.html)
* [args](https://github.com/Taywee/args)
* [dimcli](https://github.com/gknowles/dimcli)
* etc.
