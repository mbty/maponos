#!/bin/bash

# Arguments count verification
if [ $# -ne 1 ]; then
  echo "Precisely one argument expected (allowed values: \"minimal\", \"minimal_lib\", \"advanced\")."
  exit 1
fi

# Move to the example folders
cd ../examples

# Arguments validity verification
example_folder="example_$1/maponos_configuration"
if [ ! -d ${example_folder} ]; then
  echo "$1 does not correspond to an existing example (\"minimal\", \"minimal_lib\" and \"advanced\" should exist)."
  exit 2
fi

# Previous example cleanup
rm -rf ../../maponos_configuration
if [ $? -ne 0 ]; then
  echo "The removal of the previously activated example (if existing) caused an error (code: $?)."
  exit 3
fi

# New example setup
cp -r ${example_folder} ../..
if [ $? -ne 0 ]; then
  echo "The copy of the new example to its expected position ran into an error (code: $?)."
  exit 4
fi

# Compilation
cd ../build
cmake ..

# Can't compile example without a main
if [[ $1 =~ ^.*_lib$ ]]; then
  # Ensures that the maponos_example target is removed
  rm -rf maponos_example
  make
else
  # Compile with main
  make maponos_example
fi;
