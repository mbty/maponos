# Build

## Prerequisites
* [CMake](https://cmake.org)
* [Make](https://gnu.org/software/make)
* A C++ compiler with C++17 support

## Available targets
default target: compile to lib
`maponos_example`: compile example

## Example loading
Use the `load_example.sh` script with one of the allowed values (which should
be `minimal`, `minimal_lib` and `advanced`). Loading an example removes and
recreates the `../../maponos_configuration` folder so be somewhat careful.

## Linux/Mac
### First compilation
```sh
cmake ..
make <target name>
```

### Subsequent compilations
```sh
make <target name>
```
